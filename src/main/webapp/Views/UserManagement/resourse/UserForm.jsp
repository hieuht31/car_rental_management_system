<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<main>
	<h1>User ${param.action}</h1>
	<hr>
	<form action="${param.action}" method="post" id="form">
		<c:if test="${requestScope.user_update != null}">
			<input type="hidden" id="userId" value="${requestScope.user_update.userId}"
				name="userId" />
		</c:if>
		<div class="input-box">
			<label for="full-name">Full Name<span>(*)</span></label> <input
				type="text" id="full-name" name="fullName"
				<c:if test="${requestScope.user_update != null}">value="${user_update.fullName}"</c:if>
				required placeholder="Full Name" >
		</div>
		<div class="input-box">
			<label for="dob">DoB<span>(*)</span></label> <input
				type="date" id="dob" name="dateOfBirth"
				<c:if test="${requestScope.user_update != null}">value="${user_update.dateOfBirth}"</c:if>
				required>
		</div>
		<div class="input-box">
			<label for="address">Address<span>(*)</span></label> <input
				type="text" id="address" name="address"
				<c:if test="${requestScope.user_update != null}">value="${user_update.address}"</c:if>
				required placeholder="Address">
		</div>
		<div class="input-box">
			<label for="phoneNumber">Phone number<span>(*)</span></label> <input
				type="text" id="phoneNumber" name="phoneNumber"
				<c:if test="${requestScope.user_update != null}">value="${user_update.phoneNumber}"</c:if>
				required placeholder="Phone number">
		</div>
		<div class="input-box">
			<label for="email">Email<span>(*)</span></label>
			<div class="input-element">
				<input placeholder="Email" type="email" id="email" name="email" <c:if test="${requestScope.user_update != null}">value="${user_update.email}" disabled</c:if>
				 autocomplete="false"/>
			</div>
		</div>
		<div class="input-box">
			<label for="password">Password<span>(*)</span></label>
			<div class="input-element">
				<input placeholder="Password" type="password" id="password" name="password" <c:if test="${requestScope.user_update != null}">value="${user_update.password}" disabled</c:if>
				 autocomplete="false"/>
			</div>
		</div>
		<div class="input-box">
			<label for="role">Role<span>(*)</span></label>
			<div class="input-element">
				<select name="role" id="role" required>
				<c:if test="${requestScope.user_update != null}"><option value="${requestScope.user_update.role}" hidden>${requestScope.user_update.role}</option></c:if>
						<option value="Admin">Admin</option>
						<option value="Staff">Staff</option>
						<option value="Customer">Customer</option>
				</select>
			</div>
		</div>
		<div class="input-box">
			<label for="status">Status<span>(*)</span></label>
			<div class="input-element">
				<select name="status" id="status" required>
				<c:if test="${requestScope.user_update != null}"><option value="${requestScope.user_update.status}" hidden>${requestScope.user_update.status}</option></c:if>
						<option value="Active">Active</option>
						<option value="InActive">Inactive</option>
				</select>
			</div>
		</div>
		<footer>
			<button class="btn btn-m warning" type="reset">
				<i class="fas fa-undo"></i>Reset
			</button>
			<button class="btn btn-m success" type="button" id="btn-${param.action}">
				<i class="fas fa-plus"></i><span class="up-first">${param.action}</span>
			</button>
			<c:if test="${requestScope.user_update != null}">
				<button class="btn btn-m danger" type="button" id="btn-delete">
						<i class="fas fa-trash"></i>Delete
				</button>
			</c:if>
			<button class="btn btn-m primary" type="button" id="btn-back">
				<i class="fas fa-arrow-left"></i>Back
			</button>
		</footer>
	</form>
</main>