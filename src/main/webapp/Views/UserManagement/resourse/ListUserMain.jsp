<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<main>
	<h1>User List</h1>
	<hr>
	<div class="search-container">
		<form action="#" method="post">
			<div class="search-field search-group">
				<i class="fas fa-search"></i> <input type="search"
					name="searchString" value="">
			</div>
			<div class="filter-field search-group">
				<label for="filter-type"><i class="fas fa-filter"></i>Filter
					by</label><select class="select-text-overflow" name="filterBy"
					id="filter-type" data-selected="">
					<option value="full_name">Full Name</option>
					<option value="dob">DoB</option>
					<option value="address">Address</option>
					<option value="phone_number">Phone number</option>
					<option value="role">Role</option>
				</select>
			</div>
			<button class="filter-submit" onclick="filter()" type="button">Search</button>
		</form>
	</div>
	<table id="table-result">
		<thead>
			<tr>
				<td>No</td>
				<td>Full name</td>
				<td>DoB</td>
				<td>Address</td>
				<td>Phone Number</td>
				<td>Role</td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody>

		</tbody>
	</table>
	<div class="table-pagination" style="margin-top: 50px;">
		<ul class="pagination" id="pagination">
		</ul>
	</div>
</main>