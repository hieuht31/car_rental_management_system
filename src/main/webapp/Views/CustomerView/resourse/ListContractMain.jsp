<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<main>
	<h1>Trip List</h1>
	<hr>
	<div class="search-container">
		<form action="#" method="post">
			<div class="search-field search-group">
				<i class="fas fa-search"></i> <input type="search"
					name="searchString" value="">
			</div>
			<div class="filter-field search-group">
				<label for="filter-type"><i class="fas fa-filter"></i>Filter
					by</label><select class="select-text-overflow" name="filterBy"
					id="filter-type" data-selected="">
					<option value="create_at">Create at</option>
					<option value="start_time" data-type='time'>Start date</option>
					<option value="end_time">End date</option>
					<option value="customer">Customer</option>
					<option value="status" data-type='number'>Status</option>
				</select>
			</div>
			<button class="filter-submit" onclick="filter()" type="button">Search</button>
		</form>
	</div>
	<table id="table-result">
		<thead>
			<tr>
				<td>No</td>
				<td>Create At</td>
				<td>Start Date</td>
				<td>End Date</td>
				<td>Customer</td>
				<td>Status</td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody>

		</tbody>
	</table>
	<div class="table-pagination" style="margin-top: 50px;">
		<ul class="pagination" id="pagination">
		</ul>
	</div>
</main>