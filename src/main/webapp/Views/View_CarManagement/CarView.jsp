<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!--<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content= "width=device-width, initial-scale=1.0">-->
<title>Car List</title>
<jsp:include page="../../Components/headLink.jsp"></jsp:include>
<link rel="stylesheet" href="../resourse/css/common-style.css">
</head>
<body>
	<jsp:include page="../../Components/confirm-box.jsp" />
	<jsp:include page="../../Components/header.jsp">
		<jsp:param value="Car manager" name="logo" />
		<jsp:param value="fas fas fa-car" name="classes" />
	</jsp:include>
<%--  
	<c:if test="${user.role == 'Staff'}"></c:if>--%>
		<jsp:include page="../../Components/nav-left-aside-staff.jsp">
			<jsp:param value="1" name="currentIndex" />
	</jsp:include>	
	

	<jsp:include page="resourse/CarViewMain.jsp" />
	<jsp:include page="../../Components/scriptLink.jsp"></jsp:include>
	<script src="../resourse/js/fetch-api.js"></script>
	<script src="../resourse/js/form-handler.js"></script>
	<script src="../resourse/js/repository.js"></script>

</body>
</html>