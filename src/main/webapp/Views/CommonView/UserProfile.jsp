<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>User Profile</title>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.0/css/all.min.css"
	integrity="sha512-3PN6gfRNZEX4YFyz+sIyTF6pGlQiryJu9NlGhu9LrLMQ7eDjNgudQoFDK3WSNAayeIKc6B8WXXpo4a7HqxjKwg=="
	crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="../resourse/css/common-style.css">
<link rel="stylesheet" href="../resourse/css/form-style.css">
</head>
<body>
	<jsp:include page="../../Components/confirm-box.jsp" />
	<jsp:include page="../../Components/header.jsp">
		<jsp:param value="User" name="logo" />
		<jsp:param value="fas fa-users" name="classes" />
		<jsp:param name="userName" value="Service staff" />
	</jsp:include>
	<jsp:include page="../../Components/nav-left-aside-common.jsp">
		<jsp:param value="2" name="currentIndex" />
	</jsp:include>


	<main>
		<h1>Trip add</h1>
		<hr>
		<form action="add" method="post" id="form">
			<input type="hidden" name="userId" value="${user_info.userId}">
			<div class="input-box">
				<label for="fullName">Full Name<span>(*)</span></label> <input
					type="text" id="fullName" name="fullName" required
					placeholder="Full Name" value="${user_info.fullName}">
			</div>
			<div class="input-box">
				<label for="dob">Date of birth<span>(*)</span></label> <input
					type="date" id="dob" name="dateOfBirth" required
					value="${user_info.dateOfBirth}">
			</div>
			<div class="input-box">
				<label for="address">Address<span>(*)</span></label> <input
					type="text" id="address" name="address" required
					placeholder="Address" value="${user_info.address}" required>
			</div>
			<div class="input-box">
				<label for="phoneNumber">Phone number<span>(*)</span></label> <input
					type="text" id="phoneNumber" name="phoneNumber"
					placeholder="Phone number" required
					value="${user_info.phoneNumber}" />
			</div>
			<div class="input-box">
				<label for="email">Address<span>(*)</span></label> <input
					type="text" id="email" name="email" required placeholder="Email"
					value="${user_info.email}" disabled required>
			</div>
			<div class="input-box">
				<label for="role">Role<span>(*)</span></label> <input type="text"
					id="role" name="role" required placeholder="Email"
					value="${user_info.role}" disabled>
			</div>

			<footer>
				<button class="btn btn-m primary" type="button" id="btn-back">
					<i class="fas fa-angle-double-left"></i>Back
				</button>
				<button class="btn btn-m warning" type="reset" id="btn-reset">
					<i class="fas fa-undo"></i>Reset
				</button>
				<button class="btn btn-m success" type="submit" id="btn-update">
					<i class="fas fa-plus"></i>Update
				</button>
				<button class="btn btn-m danger" type="submit" id="btn-delete">
					<i class="fas fa-trash"></i>Delete
				</button>
			</footer>
		</form>
	</main>
	<jsp:include page="../../Components/message-container.jsp">
		<jsp:param name="successMessage" value="${successMessage}" />
		<jsp:param name="warningMessage" value="${warningMessage}" />
		<jsp:param name="errorMessage" value="${errorMessage}" />
	</jsp:include>
	<jsp:include page="../../Components/scriptLink.jsp"></jsp:include>
	<script src="../resourse/js/form-handler.js"></script>

</body>
</html>