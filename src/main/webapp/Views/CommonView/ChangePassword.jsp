<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>Change password</title>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.0/css/all.min.css"
	integrity="sha512-3PN6gfRNZEX4YFyz+sIyTF6pGlQiryJu9NlGhu9LrLMQ7eDjNgudQoFDK3WSNAayeIKc6B8WXXpo4a7HqxjKwg=="
	crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="../resourse/css/common-style.css">
<link rel="stylesheet" href="../resourse/css/form-style.css">
</head>
<body>
	<jsp:include page="../../Components/confirm-box.jsp" />
	<jsp:include page="../../Components/header.jsp">
		<jsp:param value="User" name="logo" />
		<jsp:param value="fas fa-users" name="classes" />
		<jsp:param name="userName" value="Service staff" />
	</jsp:include>
	<jsp:include page="../../Components/nav-left-aside-common.jsp">
		<jsp:param value="3" name="currentIndex" />
	</jsp:include>


	<main>
		<h1>Change password</h1>
		<hr>
		<form action="change_password" method="post" id="form">
			<input type="hidden" name="userId" value="${sessionScope.user.userId}">
			<div class="input-box">
				<label for="currentPassword">Current password<span>(*)</span></label> <input
					type="password" id="currentPassword" name="currentPassword" required
					placeholder="Current Password" />
			</div>
			<div class="input-box">
				<label for="newPass">New password<span>(*)</span></label> <input
					type="password" id="newPass" name="password" required placeholder="Password">
			</div>
			<div class="input-box">
				<label for="confirmPass">Confirm password<span>(*)</span></label> <input
					type="password" id="confirmPass" name="confirmPassword" placeholder="Confirn Password" required>
			</div>
			<footer>
				<button class="btn btn-m primary" type="button" id="btn-back">
					<i class="fas fa-angle-double-left"></i>Back
				</button>
				<button class="btn btn-m warning" type="reset" id="btn-reset">
					<i class="fas fa-undo"></i>Reset
				</button>
				<button class="btn btn-m success" type="submit" id="btn-update-pass">
					<i class="fas fa-plus"></i>Update
				</button>
			</footer>
		</form>
	</main>
	<jsp:include page="../../Components/message-container.jsp">
		<jsp:param name="successMessage" value="${successMessage}" />
		<jsp:param name="warningMessage" value="${warningMessage}" />
		<jsp:param name="errorMessage" value="${errorMessage}" />
	</jsp:include>
	<jsp:include page="../../Components/scriptLink.jsp"></jsp:include>
	<script src="../resourse/js/form-handler.js"></script>

</body>
</html>