<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="./resourse/css/toast.css">
    <style>
        *{
            padding: 0;
            margin: 0;
            box-sizing: border-box;
        }

        a{
            text-decoration: none;
            color: black;
        }



        body{
            position: relative;
            width: 100vw;
            height: 100vh;
        }
        div.wall-paper{
            position: absolute;
            width: 45%;
            height: 100%;
            left: 0%;
            position: absolute;
            overflow: hidden;
            
            /* top: -6%;
            bottom: -11%; */

            background: url(https://i.pinimg.com/originals/0b/e7/c8/0be7c85210ea699c67554cf1f72edbeb.jpg);
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }

        div.wall-paper::before{
            content: '';
            position: absolute;
            left: 0%;
            top: 0%;
            bottom: 21.5%;
            width: 100%;
            height: calc(100% - 21.5%);
            background: radial-gradient(50% 50% at 50% 50%, rgb(0 0 0 / 0%) 0%, rgb(0 0 0) 100%);
        }
        .wall-paper::after{
            content: 'CR, with you on every journey';
            position: absolute;
            color: white;
            font-size: 38px;
            text-align: center;
            left: 0%;
            top: 78.5%;
            bottom: 0%;
            width: 100%;
            height:21.5%;

            background: radial-gradient(50% 50% at 50% 50%, rgb(0 0 0 / 73%) 0%, rgb(0 0 0) 100%);
        }
        
        .wall-paper a {
            position: relative;
            font-size: 24px;
            left: 60px;
            top: 30px;
            color: white;
            z-index: 1;
        }
        .wall-paper a::before{
            content: '';
            position: absolute;
            width: 3px;
            border: 3px;
            height: 15px;
            left: -18px;
            transform: rotate(45deg);
            background-color: white;
        }

        .wall-paper a::after{
            content: '';
            position: absolute;
            width: 3px;
            border: 3px;
            height: 15px;
            left: -18px;
            top: 9px;
            transform: rotate(-45deg);
            background-color: white;
        }
        .register-box{
            position: relative;
            width: 55%;
            height: 100%;
            left: 45%;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
        }

        input:not(input[type='checkbox']){
            width: 630px;
            padding: 15px;
            margin: 30px 15px;
            border-radius: 15px;
            font-size: 18px;
        }
        
        div:has(label){
            display: flex;
            align-content: center;
        }

        div:has(.btn){
            display: flex;
            justify-content: center;
        }
        .group-input-box{
            display: flex;
        }

        .group-input-box input:not(input[type='checkbox']){
            width: 300px;
            padding: 15px;
            margin: 30px 15px;
            border-radius: 15px;
            font-size: 18px;
        }
        input[type="checkbox"]{
            margin-left: 20px;
            margin-right: 5px;
            padding: 10px;
            width: 18px;
            height: 18px;

        }
        label{
            font-size: 18px;
        }

        .register-box .btn{

            width: 100%;
            padding: 13px;
            font-size: 24px;
            color: white;
            text-align: center;
            vertical-align: middle;
            background: #94A1B5;
            border-radius: 15px;
            cursor: pointer;
        }

        .btn:hover{
            background: #5478af; 
        }

        footer{
            width: 100%;
        }
        footer div{
            margin: 8px 60px;
            font-size: 18px;
        }
    </style>
    
</head>
<body>
    <div class="wall-paper"><a href="login">Back to login</a></div>
    <div class="register-box">
        <h1>Sign up</h1>
        <form action="register" method="post">
            <div class="group-input-box">
                <div><input type="text" name="fullName" placeholder="Full Name" required/></div>
                <div><input type="date" name="dateOfBirth" placeholder="Date of birth" required/></div>
            </div>
            <div class="group-input-box">
                <div><input type="text" name="phoneNumber" placeholder="Phone number" required/></div>
                <div><input type="text" name="address" placeholder="Address" required/></div>
            </div>
            <div><input type="text" name="email" placeholder="Email" required/></div>
            <div class="group-input-box">
              <div><input type="password" name="password" placeholder="Password" required/></div>
            <div><input type="password" name="confirmPassword" placeholder="Confirm password" required/></div>
            </div>
            <div><button class="btn">Sign Up</button></div>
        </form>
    </div>
    <jsp:include page="../../Components/message-container.jsp"/>
    <script src="resourse/js/common.js"></script>
</body>
</html>