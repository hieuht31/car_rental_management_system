<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="resourse/css/common-style.css">
</head>
<body id="flex">
	<h1>Access Denied!</h1>
	<p>
		Oops, you can't access this page <span id="btn-back">Click here
			to back</span>
	</p>
	<jsp:include page="../../Components/scriptLink.jsp"></jsp:include>
	<script src="resourse/js/form-handler.js"></script>
</body>
</html>