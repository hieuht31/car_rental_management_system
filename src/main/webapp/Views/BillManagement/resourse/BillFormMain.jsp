<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<main>
	<h1><span class="up-first">${param.action}</span> Car</h1>
	<hr>
	<form action="${param.action}" method="post" id="form"> 
		<c:if test="${requestScope.car != null}">
			<input type="hidden" value="${requestScope.car.carId}"
				name="carId" />
		</c:if>
		
		<div class="input-box ">
			<label for="license_plate">License plate<span>(*)</span></label> <input
				type="text" id="license_plate" name="licensePlate"
				<c:if test="${requestScope.car!= null}">value="${requestScope.car.licensePlate}"</c:if>
				required>
		</div> 
		<div class="input-box ">
			<label for="car_type">Car type<span>(*)</span></label> <input
				type="text" id="car_type" name="carType"
				<c:if test="${requestScope.car!= null}">value="${car.carType}"</c:if>
				required>
		</div>
		<div class="input-box ">
			<label for="car_color">Car color<span>(*)</span></label> <input
				type="text" id="car_color" name="carColor"
				<c:if test="${requestScope.car!= null}">value="${car.carColor}"</c:if>
				required>
			<p class="error-message">Car color is invalid</p>
		</div>
		<div class="input-box ">
			<label for="carBrand">Car Brand<span>(*)</span></label> <input
				type="text" id="carBrand" name="carBrand"
				<c:if test="${requestScope.car!= null}">value="${requestScope.car.carBrand}"</c:if>
				required>
		</div>
		<div class="input-box">
			<label for="rentalRate">Rental Rate<span>(*)</span></label> <input
				type="text" id="rentalRate" name="rentalRate"
				<c:if test="${requestScope.car!= null}">value="${requestScope.car.rentalRate}"</c:if>
				required>
		</div>
		<div class="input-box">
			<label for="capacity">Seating Capacity<span>(*)</span></label> <input
				type="text" id="capacity" name="capacity"
				<c:if test="${requestScope.car!= null}">value="${requestScope.car.capacity}"</c:if>
				required>
		</div>
		<div class="input-box">
			<label for="status">Status<span>(*)</span></label>
			<div class="input-element">
				<select name="status" id="status" required>
				<c:if test="${requestScope.user_update != null}"><option value="${requestScope.user_update.status}" hidden>${requestScope.user_update.status}</option></c:if>
						<option value="Active">Active</option>
						<option value="InActive">Inactive</option>
				</select>
			</div>
		</div>
		<footer>
			<button class="btn btn-m" id="btn-back" type="button">
				<i class="fas fa-angle-double-left"></i>Back
			</button>
			<button class="btn btn-m success" id="btn-${param.action}" type="button">
				<i class="fas fa-plus"></i><span class="up-first">${param.action}</span>
			</button>
			<button class="btn btn-m warning" type="reset">
				<i class="fas fa-undo"></i>Reset
			</button>
		</footer>
	</form>
</main>