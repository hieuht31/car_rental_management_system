document.querySelectorAll(".drop-nav").forEach((dropNav) => {
	dropNav.querySelector(".drop-nav-title").onclick = () => {
		dropNav.classList.toggle("drop");
	};
});

function checkValidSearch(filterBy, searchValue) {
	if (filterBy == 'date') {
		// check valid date format dd/mm/yyyy
		let dateRegex = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
		if (!dateRegex.test(searchValue)) {
			Swal.fire({
				icon: 'warning',
				title: 'Invalid date format',
				text: 'Please enter date in format dd/mm/yyyy'
			});
			return false;
		} else {
			return true;
		}
	}
	if(filterBy == 'number'){
		if(/^(?=.*[\d])[\d-.][\d.]*$/.test(searchValue)==false){
			Swal.fire({
				icon: 'warning',
				title: 'Invalid number format',
				text: 'Please enter a number'
			});
			return false;
		} else if (searchValue.startsWith('-')){
			Swal.fire({
				icon: 'warning',
				title: 'Invalid number value',
				text: 'Please enter a positive number'
			});
			return false;
		} else return true;
	}
	if(filterBy == 'time'){
		
		if(/^([0-9]{2}):([0-9]{2}):([0-9]{2})$/.test(searchValue)==false){
			Swal.fire({
				icon: 'warning',
				title: 'Invalid time format',
				text: 'Please enter a with format hh:mm:ss'
			});
			return false;
		}  else return true;
	} 
	return true;
}

function parseDate(dateValue){
	let split = dateValue.split('/*');
	return `${split[2]}-${split[1]}-${split[0]}`
}

function activeDelete(){
		console.log("he")
	$(".action-btn.delete").each(function() {
		console.log($(this))
		$(this).click(function(e) {
			e.preventDefault();
			// swal button confirm
			Swal.fire({
				title: 'Are you sure?',
				text: "Do you want to delete this employee?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			}).then((result) => {
				if (result.isConfirmed) {
					$.ajax({
						type: "POST",
						url: $(this).attr('href'),
						success: function(data) {
							if (data.success) {
								Swal.fire({
									icon: 'success',
									title: data.message,
									showConfirmButton: false,
									timer: 1000
								}),
									// reload page after 1s
									setTimeout(function() {
										location.href = "list";
									}, 1000);
							} else {
								Swal.fire({
									icon: 'error',
									title: data.message,
									timer: 1000
								});
							}
						}
					});
				}
			});
		});
	});
}
/**
 * fetchData 
 */

$(document).ready(function() {
	getFilterParams();
});

$(document).on('click', '#pagination li', function() {
	var page = $(this).attr('attr-page');
	if (page != undefined && page != "") {
		getFilterParams(Number(page));
	}
});

function filter() {
	getFilterParams();
}
$('input[name="date-filter"]').change(()=>{
	console.log('change')
	filter();
})


function getFilterParams(pageNum) {
	let selected = document.querySelector('select[name="filterBy"]').selectedOptions[0];
	let filterType = selected.dataset.type==undefined?'':selected.dataset.type
	let filterBy = $('select[name="filterBy"]').val();
	console.log(selected+" "+filterType);
	let searchValue = $('input[name="searchString"]').val();
	let numberRecord = 5;
	let currentPage = pageNum || 1;
	if (checkValidSearch(filterType, searchValue) == false) {
		return;
	} else {
		if ($('input[name ="date-filter"]').val() != undefined && $('input[name ="date-filter"]').val() != '') {
			searchValue = $('input[name="searchString"]').val() + '&' + $('input[name="date-filter"]').val();;
		}
		getDataTable(currentPage, numberRecord, searchValue, filterBy);
	}
}
function renderPaging(currentPage, totalPage) {
	const pageCaculate = pages(currentPage, totalPage, 1);
	$('#pagination').empty();
	if (totalPage <= 0) return;
	$('#pagination').append(`<li class="page-item ${currentPage == 1 ? 'disabled' : ''}" attr-page=${currentPage == 1 ? 1 : (currentPage - 1)}><a class="page-link" href="#">Previous</a></li>`);
	pageCaculate.forEach(element => {
		$('#pagination').append(`<li class="page-item ${element == currentPage ? 'active' : ''} ${element == '...' ? 'disabled':''} " attr-page=${element}><a class="page-link" href="#">${element}</a></li>`);
	});
	$('#pagination').append(`<li class="page-item ${currentPage == totalPage ? 'disabled' : ''}" attr-page=${currentPage == totalPage ? totalPage : (currentPage + 1)}><a class="page-link" href="#">Next</a></li>`);
}

function getDataTable(currentPage = 1, numberRecord = 5, searchValue = '', filterBy) {
	$.ajax({
		url: "list",
		type: "POST",
		dataType: "json",
		data: {
			currentPage,
			numberRecord,
			searchValue,
			filterBy
		},
		success: function(res) {
			console.log('call')
			const data = res.data;
			renderTable(data.data);
			activeDelete();
			renderPaging(data.currentPage, data.totalPage);
		}
	});
}

function renderTable(data) {
	let page = window.location.pathname;
	console.log(page.includes('ticket'))
	if(page.includes('user')) rendUserTable(data)
	else if(page.includes('car')) rendCarTable(data)
 	else if(page.includes('park')) renderTablePark(data)
 	else if(page.includes('ticket')) renderTableTicket(data)
 	else if(page.includes('book')) rendBillTable(data)
 	
 }
function rendUserTable(data) {
	$('#table-result tbody').empty();
	if (data.length == 0) {
		$('#table-result tbody').append(`<tr><td colspan="7">No matches</td></tr>`);
		return;
	}
	else {
		data.forEach(element => {
			$('#table-result tbody').append(`
        <tr>
            <td>${element.userId}</td>
            <td>${element.fullName}</td>
            <td>${element.dateOfBirth}</td>
            <td>${element.address}</td>
            <td>${element.phoneNumber}</td>
            <td>${element.role}</td>
            <td>
                <a href="update?userId=${element.userId}" ><i class="fas fa-eye mr-5"></i>View</a>
            </td>
        </tr>
        `);
		});
	}
}
function rendContractTable(data) {
	$('#table-result tbody').empty();
	if (data.length == 0) {
		$('#table-result tbody').append(`<tr><td colspan="7">No matches</td></tr>`);
		return;
	} else {
		data.forEach(element => {
			$('#table-result tbody').append(`
        <tr>
            <td>${element.rentalContractId}</td>
            <td>${element.createAt}</td>
            <td>${element.startDate}</td>
            <td>${element.endDate}</td>
            <td>${element.user.fullName}</td>
            <td>${element.status}</td>
            <td>
                <a class="action-btn update"
						href="update?trip_id=${element.rentalContractId}"><i
							class="far fa-edit"></i>Update</a><a class="action-btn delete"
						href="delete?trip_id=${element.rentalContractId}"><i
							class="fas fa-trash-alt"></i>Delete</a></td></td>
        </tr>
        `);
		});
	}
}

function rendCarTable(data) {
	$('#table-result tbody').empty();
	if (data.length == 0) {
		$('#table-result tbody').append(`<tr><td colspan="10">No matches</td></tr>`);
		return;
	} else {
		data.forEach(element => {
			$('#table-result tbody').append(`
        <tr>
            <td>${element.carId}</td>
            <td>${element.licensePlate}</td>
            <td>${element.carType}</td>
            <td>${element.carColor}</td>
            <td>${element.carBrand}</td>
            <td>${element.rentalRate}</td>
            <td>${element.capacity}</td>
            <td>${element.status}</td>
            <td>
                <a class="action-btn update"
						href="update?carId=${element.carId}"><i
							class="far fa-edit"></i>Update</a><a class="action-btn delete"
						href="delete?carId=${element.carId}"><i
							class="fas fa-trash-alt"></i>Delete</a></td></td>
        </tr>
        `);
		});
	}
}

function rendCatalogCar(data) {
	$('#table-result tbody').empty();
	if (data.length == 0) {
		$('#table-result tbody').append(`<tr><td colspan="6">No matches</td></tr>`);
		return;
	} else {
		data.forEach(element => {
			$('#table-result tbody').append(`
        <tr>
            <td>${element.carId}</td>
            <td>${element.carType}</td>
            <td>${element.carColor}</td>
            <td>${element.carBrand}</td>
            <td>${element.rentalRate}</td>
            <td>${element.capacity}</td>
        </tr>
        `);
		});
	}
}

function rendBillTable(data){
	$('#table-result tbody').empty();
	if (data.length == 0) {
        $('#table-result tbody').append(`<tr><td colspan="6">No matches</td></tr>`);
        return;
    }else {
		
        data.forEach(element => {
            $('#table-result tbody').append(`
        <tr>
            <td>${element.billId}</td>
            <td>${element.issueDate}</td>
            <td>${element.rentalContract.user.fullName}</td>
            <td>${element.status}</td>
            <td>
                <a class="action-btn update"
						href="update?office_id=${element.billId}"><i
							class="far fa-edit"></i>Update</a><a class="action-btn delete"
						href="delete?office_id=${element.billId}"><i
							class="fas fa-trash-alt"></i>Delete</a></td></td>
        </tr>
        `);
        });
    }
}
function renderTablePark(data){
	$('#table-result tbody').empty();
	if (data.length == 0) {
        $('#table-result tbody').append(`<tr><td colspan="7">No matches</td></tr>`);
        return;
    }else {
        data.forEach(element => {
            $('#table-result tbody').append(`
        <tr>
            <td>${element.park_id}</td>
            <td>${element.park_name}</td>
            <td>${element.park_place}</td>
            <td>${element.park_area}</td>
             <td>${element.park_price}</td>
            <td>${element.park_status==0?'Active':(element.park_status==1?'Full':'Inactive')}</td>
            <td>
                <a class="action-btn update"
						href="update?park_id=${element.park_id}"><i
							class="far fa-edit"></i>Update</a><a class="action-btn delete"
						href="delete?park_id=${element.park_id}"><i
							class="fas fa-trash-alt"></i>Delete</a></td></td>
        </tr>
        `);
        });
    }
}
function renderTableTicket(data){
	$('#table-result tbody').empty();
		if (data.length == 0) {
        $('#table-result tbody').append(`<tr><td colspan="6">No matches</td></tr>`);
        return;
    }else {
        data.forEach(element => {
            $('#table-result tbody').append(`
        <tr>
            <td>${element.ticket_id}</td>
            <td>${element.trip.destination}</td>
            <td>${element.car.license_plate}</td>
            <td>${element.customer_name}</td>
            <td>${element.booking_time}</td>
            <td>
                <a class="action-btn update"
						href="update?ticket_id=${element.ticket_id}"><i
							class="far fa-edit"></i>Update</a><a class="action-btn delete"
						href="delete?ticket_id=${element.ticket_id}"><i
							class="fas fa-trash-alt"></i>Delete</a></td></td>
        </tr>
        `);
        });
    }
}




function pages(currentPage, totalPage, delta = 2) {
	let pages = [];
	for (let i = 1; i <= totalPage; i++) {
		if (i == 1 || i == totalPage || (i >= currentPage - delta && i <= currentPage + delta)) {
			pages.push(i);
		} else if (i == currentPage - (delta + 1) || i == currentPage + (delta + 1)) {
			pages.push('...');
		}
	}
	return pages;
}
/**
 * 
 */