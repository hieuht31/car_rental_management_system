<header>
	<div class="logo-container">
		<i class="${param.classes}"></i><span>${param.logo}</span>
	</div>
	<div class="content">
		<i class="fas fa-stream"></i><a href="../account/profile?userId=${sessionScope.user.userId}"><span>Welcome ${sessionScope.user.fullName}</span></a> <span><a href="../logout"><i
				class="fas fa-sign-out-alt"></i>Logout</a></span>
	</div>
</header>