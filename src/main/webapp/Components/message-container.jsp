<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<aside class="fe-toast-container">
   	<c:if test="${requestScope.successMessage != '' && requestScope.successMessage != null}">
		<div class="fe-toast fe-toast-show fe-success">
			<div class="fe-toast-header ">
				Success<i class="fas fa-times"></i>
			</div>
			<div class="fe-toast-content">${requestScope.successMessage}</div>
		</div>
   	</c:if>
   	<c:if test="${requestScope.warningMessage != '' && requestScope.warningMessage != null}">
		<div class="fe-toast fe-toast-show fe-warning">
			<div class="fe-toast-header ">
				Warning<i class="fas fa-times"></i>
			</div>
			<div class="fe-toast-content">${requestScope.warningMessage}</div>
		</div>
   	</c:if>
   	<c:if test="${requestScope.errorMessage != '' && requestScope.errorMessage != null}">
		<div class="fe-toast fe-toast-show fe-error">
			<div class="fe-toast-header ">
				Error<i class="fas fa-times"></i>
			</div>
			<div class="fe-toast-content">${requestScope.errorMessage}</div>
		</div>
   	</c:if>
     
</aside>