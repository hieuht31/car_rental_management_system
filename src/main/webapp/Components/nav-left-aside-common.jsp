<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<nav>

	<c:if test="${sessionScope.user.role == 'Admin'}">
		<li class="drop-nav-link"><i
			class="fas fa-list"></i><a href="../user/list">List User</a></li>	
	</c:if>
	<c:if test="${sessionScope.user.role == 'Staff'}">
		<li
			class="drop-nav-link"><i
			class="fas fa-list"></i><a href="../car/list">List Car</a></li>	
	</c:if>
	<c:if test="${sessionScope.user.role == 'Customer'}">
		<li
			class="drop-nav-link"><i
			class="fas fa-list"></i><a href="../rental/catalog">Catalog Car</a></li>	
	</c:if>
			<li class="drop-nav-link <c:if test="${param.currentIndex==2}">active</c:if>"><i
			class="fas fa-user-circle"></i><a href="../rental/catalog">User profile</a></li>
		<li
			class="drop-nav-link <c:if test="${param.currentIndex==3}">active</c:if>"><i
			class="fas fa-plus"></i><a href="../account/change_password">Change password</a></li>

</nav>