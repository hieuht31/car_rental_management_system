<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="confirm-box" id="confirm-box">
	<div class="confirm-header">
		<i class="fas fa-exclamation-triangle"></i>Confirm
	</div>
	<div class="confirm-message">Can't roll-back operator! Do you want to continue?</div>
	<div class="confirm-footer">
		<button type="button" class="btn btn-m danger">Yes</button>
		<button type="button" class="btn btn-m primary">No</button>
	</div>
</div>