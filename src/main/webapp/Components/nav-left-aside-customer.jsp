<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<nav>
	<ul>
		<li
			class="drop-nav drop">
			<div class="drop-nav-title">
				<i class="fas fa-car "></i>Car Rental<i class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">
				<li
					class="drop-nav-link <c:if test="${param.currentIndex==1}">active</c:if>"><i
					class="fas fa-list"></i><a href="../rental/catalog">Catalog Car</a></li>
				<li
					class="drop-nav-link <c:if test="${param.currentIndex==2}">active</c:if>"><i
					class="fas fa-plus"></i><a href="../rental/my-contract">My Rental Contract</a></li>
				<li
					class="drop-nav-link <c:if test="${param.currentIndex==3}">active</c:if>"><i
					class="fas fa-plus"></i><a href="../rental/my-bill">My Bill</a></li>
			</ul>
		</li>
	</ul>
</nav>