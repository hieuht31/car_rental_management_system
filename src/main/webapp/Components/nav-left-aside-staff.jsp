<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<nav>
	<ul>
		<li
			class="drop-nav <c:if test="${param.currentIndex>0 && param.currentIndex<3}">drop</c:if>">
			<div class="drop-nav-title">
				<i class="fas fa-car "></i>Car manager<i class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">
				<li
					class="drop-nav-link <c:if test="${param.currentIndex==1}">active</c:if>"><i
					class="fas fa-list"></i><a href="../car/list">Car list</a></li>
				<li
					class="drop-nav-link <c:if test="${param.currentIndex==2}">active</c:if>"><i
					class="fas fa-plus"></i><a href="../car/add">Add Car</a></li>
			</ul>
		</li>
		<li
			class="drop-nav <c:if test="${param.currentIndex>2 && param.currentIndex<5}">drop</c:if>">
			<div class="drop-nav-title">
				<i class="fas fa-cart-plus"></i>Contract Management<i
					class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">
				<li
					class="drop-nav-link <c:if test="${param.currentIndex==3}">active</c:if>"><i
					class="fas fa-list"></i><a href="../contract/list">Contract list</a></li>
				<li
					class="drop-nav-link <c:if test="${param.currentIndex==4}">active</c:if>"><i
					class="fas fa-plus"></i><a href="../contract/add">Add contract</a></li>
			</ul>
		</li>
		<li class="drop-nav <c:if test="${param.currentIndex>4}">drop</c:if>">
			<div class="drop-nav-title">
				<i class="fas fa-map-marker-alt"></i>Bill management<i
					class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">
				<li
					class="drop-nav-link <c:if test="${param.currentIndex==5}">active</c:if>"><i
					class="fas fa-list"></i><a href="../park/view">Bill list</a></li>
				<li
					class="drop-nav-link <c:if test="${param.currentIndex==6}">active</c:if>"><i
					class="fas fa-plus"></i><a href="../park/add">Add Bill</a></li>
			</ul>
		</li>
	</ul>
</nav>