<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<nav>
	<ul>
		<li class="drop-nav repo">
			<div class="drop-nav-title">
				<i class="fas fa-plane rotate"></i>Welcome
				${sessionScope.employee.employee_name}<i class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">
				<li class="drop-nav-link"><a
					href="../user/profile?employee_id=${sessionScope.employee.employee_Id}"><i
						class="fas fa-list"></i>User profile</a></li>
				<li class="drop-nav-link"><a href="../LogoutController"><i
						class="fas fa-plus"></i>Logout</a></li>
			</ul>
		</li>
		<li
			class="nav-link <c:if test="${param.currentIndex==1}">active</c:if>"><i
			class="fas fa-tachometer-alt"></i><a>Car parking manager</a></li>
		<li
			class="drop-nav <c:if test="${param.currentIndex>1 && param.currentIndex<4}">drop</c:if>">
			<div class="drop-nav-title">
				<i class="fas fa-car "></i>Car manager<i class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">
				<li
					class="drop-nav-link <c:if test="${param.currentIndex==2}">active</c:if>"><i
					class="fas fa-list"></i><a href="../car/view">Car list</a></li>
				<li
					class="drop-nav-link <c:if test="${param.currentIndex==3}">active</c:if>"><i
					class="fas fa-plus"></i><a href="../car/add">Add Car</a></li>
			</ul>
		</li>
		<li
			class="drop-nav <c:if test="${param.currentIndex>3 && param.currentIndex<6}">drop</c:if>">
			<div class="drop-nav-title">
				<i class="fas fa-cart-plus"></i>Booking office manager<i
					class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">
				<li
					class="drop-nav-link <c:if test="${param.currentIndex==4}">active</c:if>"><i
					class="fas fa-list"></i><a href="../booking/view">Booking
						office list</a></li>
				<li
					class="drop-nav-link <c:if test="${param.currentIndex==5}">active</c:if>"><i
					class="fas fa-plus"></i><a href="../booking/add">Add Booking
						office</a></li>
			</ul>
		</li>
		<li class="drop-nav <c:if test="${param.currentIndex>5}">drop</c:if>">
			<div class="drop-nav-title">
				<i class="fas fa-map-marker-alt"></i>Parking lot manager<i
					class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">
				<li
					class="drop-nav-link <c:if test="${param.currentIndex==6}">active</c:if>"><i
					class="fas fa-list"></i><a href="../park/view">Parking lot list</a></li>
				<li
					class="drop-nav-link <c:if test="${param.currentIndex==7}">active</c:if>"><i
					class="fas fa-plus"></i><a href="../park/add">Add Parking lot</a></li>
			</ul>
		</li>
	</ul>
</nav>