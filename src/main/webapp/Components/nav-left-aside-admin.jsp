<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<nav>
	<ul>
		<li class="drop-nav drop">
			<div class="drop-nav-title">
				<i class="fas fa-user"></i>User manager<i
					class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">

				<li class="drop-nav-link ${param.currentIndex==1?'active':''}"><a
					href="../user/list"><i class="fas fa-list"></i>List User</a></li>
				<li class="drop-nav-link ${param.currentIndex==2?'active':''}"><a
					href="../user/add"><i class="fas fa-plus"></i>Add User</a></li>
			</ul>
		</li>
	</ul>
</nav>