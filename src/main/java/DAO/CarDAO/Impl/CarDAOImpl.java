package DAO.CarDAO.Impl;

import java.security.spec.RSAKeyGenParameterSpec;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import DAO.DBConnection;
import DAO.CarDAO.ICarDAO;
import Model.Car;
import Model.BussinessModel.FilterEntity;
import Model.BussinessModel.ResponseEntity;

public class CarDAOImpl extends DBConnection implements ICarDAO {
	PreparedStatement pstate;
	ResultSet rs;
	String sql;
	List<Car> carList;
	
	@Override
	public ResponseEntity findAllCar(FilterEntity filterEntity) {
		int startRecord = filterEntity.getNumberRecord() * (filterEntity.getCurrentPage() - 1);
		ResponseEntity responseEntity = new ResponseEntity();
		responseEntity.setCurrentPage(filterEntity.getCurrentPage());
		String searchCondition = filterEntity.getSearchString().isEmpty()?"": "WHERE "+filterEntity.getSearchField()+" LIKE '%'+?+'%'";
		
		carList = new ArrayList<>();
		sql = "SELECT *, CEILING(CAST(COUNT(*) OVER() AS FLOAT)/?)[number_record] FROM car "
		+searchCondition+" ORDER BY car_id "
				+ "OFFSET ? ROW FETCH NEXT ? ROWS ONLY";
		try {
			int index = 1;
			pstate = getConnection().prepareStatement(sql);
			pstate.setInt(index++, filterEntity.getNumberRecord());
			if (!searchCondition.isEmpty())
				pstate.setString(index++, filterEntity.getSearchString());
			pstate.setInt(index++, startRecord);
			pstate.setInt(index++, filterEntity.getNumberRecord());
			rs = pstate.executeQuery();
			if(rs.next()) {
				carList.add(new Car(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getDouble(6), rs.getInt(7),rs.getString(8)));
				responseEntity.setTotalPage(rs.getInt(9));
			}
			while (rs.next()) {
				carList.add(new Car(rs.getInt(1), rs.getString(2), 
						rs.getString(3), rs.getString(4), rs.getString(5), 
						rs.getDouble(6), rs.getInt(7),rs.getString(8)));				
			}
			responseEntity.setData(carList);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return responseEntity;
	}

	@Override
	public ResponseEntity findAvailableCar(FilterEntity filterEntity) {
		int startRecord = filterEntity.getNumberRecord() * (filterEntity.getCurrentPage() - 1);
		ResponseEntity responseEntity = new ResponseEntity();
		responseEntity.setCurrentPage(filterEntity.getCurrentPage());
		String searchCondition = filterEntity.getSearchString().isEmpty()?"": "AND "+filterEntity.getSearchField()+" LIKE '%'+?+'%'";
		
		carList = new ArrayList<>();
		sql = "SELECT *, CEILING(CAST(COUNT(*) OVER() AS FLOAT)/?)[number_record] FROM car WHERE status = 'Active' "
				+searchCondition+" ORDER BY car_id OFFSET ? ROW FETCH NEXT ? ROWS ONLY";
		try {
			int index = 1;
			pstate = getConnection().prepareStatement(sql);
			pstate.setInt(index++, filterEntity.getNumberRecord());
			if (!searchCondition.isEmpty())
				pstate.setString(index++, filterEntity.getSearchString());
			pstate.setInt(index++, startRecord);
			pstate.setInt(index++, filterEntity.getNumberRecord());
			rs = pstate.executeQuery();
			if(rs.next()) {
				carList.add(new Car(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)
						, rs.getDouble(6), rs.getInt(7),rs.getString(8)));
				responseEntity.setTotalPage(rs.getInt(9));
			}
			while (rs.next()) {
				carList.add(new Car(rs.getInt(1), rs.getString(2), 
						rs.getString(3), rs.getString(4), rs.getString(5), 
						rs.getDouble(6), rs.getInt(7),rs.getString(8)));				
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return responseEntity;
	}

	
	@Override
	public Car findCarById(int carId) {
		sql = "SELECT * FROM car where car_id = ?";
		try {
			pstate = getConnection().prepareStatement(sql);
			pstate.setInt(1,carId);
			rs = pstate.executeQuery();
			if(rs.next()) {
				return new Car(rs.getInt(1), rs.getString(2), rs.getString(3), 
						rs.getString(4), rs.getString(5), 
						rs.getDouble(6), rs.getInt(7),rs.getString(8));
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	
	@Override
	public void addCar(Car car) {
		sql = "INSERT INTO car VALUES(?,?,?,?,?,?,?)";
		try {
			pstate = getConnection().prepareStatement(sql);
			
			pstate.setString(1, car.getLicensePlate());
			pstate.setString(2, car.getCarType());
			pstate.setString(3, car.getCarColor());
			pstate.setString(4, car.getCarBrand());
			pstate.setDouble(5, car.getRentalRate());
			pstate.setInt(6, car.getCapacity());
			pstate.setString(7, car.getStatus());
			
			pstate.executeUpdate();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Override
	public void updateCar(Car car) {
		sql = "UPDATE [car] "
				+ "   SET [lisence_plate] = ? "
				+ "      ,[car_type] = ? "
				+ "      ,[car_color] = ? "
				+ "      ,[car_brand] = ? "
				+ "      ,[rental_rate] = ? "
				+ "      ,[seating_capacity] = ? "
				+ "      ,[status] = ? "
				+ " WHERE car_id = ? ";
		try {
			pstate = getConnection().prepareStatement(sql);
			
			pstate.setString(1, car.getLicensePlate());
			pstate.setString(2, car.getCarType());
			pstate.setString(3, car.getCarColor());
			pstate.setString(4, car.getCarBrand());
			pstate.setDouble(5, car.getRentalRate());
			pstate.setInt(6, car.getCapacity());
			pstate.setString(7, car.getStatus());
			pstate.setInt(8, car.getCarId());
			pstate.executeUpdate();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Override
	public void updateCarStatus(int carId, String status) {
		sql = "UPDATE [car] "
				+ "   SET [status] = ? "
				+ " WHERE car_id = ? ";
		try {
			pstate = getConnection().prepareStatement(sql);
			
			pstate.setString(1, status);
			pstate.setInt(2, carId);
			pstate.executeUpdate();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Override
	public void deleteCar(Car car) {
		// TODO Auto-generated method stub
		sql = "DELETE FROM car WHERE car_id = ?";
		try {
			pstate = getConnection().prepareStatement(sql);
			
			pstate.setInt(1, car.getCarId());
			pstate.executeUpdate();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Override
	public Car findCarByLisencePlate(String lisencePlate) {
		
		sql = "SELECT * FROM car where lisence_plate = ?";
		try {
			pstate.setString(1,lisencePlate);
			rs = pstate.executeQuery();
			if(rs.next()) {
				return new Car(rs.getInt(1), rs.getString(2), rs.getString(3), 
						rs.getString(4), rs.getString(5), 
						rs.getDouble(6), rs.getInt(7),rs.getString(8));
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return null;
	}


}
