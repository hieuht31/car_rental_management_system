package DAO.CarDAO;

import java.util.List;

import Model.Car;
import Model.User;
import Model.BussinessModel.FilterEntity;
import Model.BussinessModel.ResponseEntity;

public interface ICarDAO {
	
	/**
	 * Tìm kiếm car với các tiêu chí được định nghĩa trong đối tượng FilterEntity và trả về danh sách các ResponseEntity.
	 * 
	 * @param filterEntity đối tượng FilterEntity đại diện cho các tiêu chí tìm kiếm
	 * @return List<ResponseEntity> đại diện cho các xe được tìm thấy
	 */
	public ResponseEntity findAllCar(FilterEntity filterEntity);
	
	/**
	 * Tìm kiếm car có trạng thái active (Sẵn sàng cho thuê) với các tiêu chí được định nghĩa 
	 * trong đối tượng FilterEntity và trả về danh sách các ResponseEntity.
	 * 
	 * @param filterEntity đối tượng FilterEntity đại diện cho các tiêu chí tìm kiếm
	 * @return List<ResponseEntity> đại diện cho các car được tìm thấy
	 */
	public ResponseEntity findAvailableCar(FilterEntity filterValue);
	
	/**
	 * Tìm kiếm car với ID truyền vào và trả về car có ID tương ứng
	 * 
	 * @param carId ID của Car đại diện cho tiêu chí tìm kiếm
	 * @return Car đại diện cho car được tìm thấy
	 */
	public Car findCarById(int carId);

	/**
	 * Tìm kiếm car với ID truyền vào và trả về car có ID tương ứng
	 * 
	 * @param carId ID của Car đại diện cho tiêu chí tìm kiếm
	 * @return Car đại diện cho car được tìm thấy
	 */
	public Car findCarByLisencePlate(String lisencePlate);


    /**
     * Thêm một người dùng mới vào cơ sở dữ liệu.
     *
     * @param user Người dùng mới.
     * @ Nếu có lỗi khi truy vấn cơ sở dữ liệu.
     */
    void addCar(Car car) ;

    /**
     * Cập nhật thông tin của một xe trong cơ sở dữ liệu.
     *
     * @param car Car cần cập nhật.
     */
    void updateCar(Car car);
    
    /**
     * Cập nhật trang thai của một xe trong cơ sở dữ liệu.
     *
     * @param carid Id của Car cần cập nhật.
     */
    void updateCarStatus(int carId,String status) ;

    /**
     * Xóa một người dùng khỏi cơ sở dữ liệu.
     *
     * @param user Người dùng cần xóa.
     */
    void deleteCar(Car car) ;

}
