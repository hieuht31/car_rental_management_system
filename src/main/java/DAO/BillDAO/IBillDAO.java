package DAO.BillDAO;

import java.util.List;

import Model.Bill;
import Model.Car;
import Model.BussinessModel.FilterEntity;
import Model.BussinessModel.ResponseEntity;

public interface IBillDAO {
	/**
	 * Tìm kiếm Bill với các tiêu chí được định nghĩa trong đối tượng FilterEntity và trả về danh sách các ResponseEntity.
	 * 
	 * @param filterEntity đối tượng FilterEntity đại diện cho các tiêu chí tìm kiếm
	 * @return List<ResponseEntity> đại diện cho các xe được tìm thấy
	 */
	public ResponseEntity findAllBill(FilterEntity filterEntity);
	
	/**
	 * Tìm kiếm Bill của từng khách hàng với các tiêu chí được định nghĩa 
	 * trong đối tượng FilterEntity và trả về danh sách các ResponseEntity.
	 * 
	 * @param filterEntity đối tượng FilterEntity đại diện cho các tiêu chí tìm kiếm
	 * @return List<ResponseEntity> đại diện cho các Bill được tìm thấy
	 */
	public ResponseEntity findPersonalBill(int customerId,FilterEntity filterValue);
	
	/**
     * Thêm bill mới vào cơ sở dữ liệu
     *
     * @param bill Thông tin của bill cần cập nhật.
     */
    public void addBill(Bill bill);
    
    /**
     * Cập nhật trang thai của một Bill trong cơ sở dữ liệu.
     *
     * @param billId của Bill cần cập nhật.
     * @param status của Bill cần cập nhật.
     */
    public void updateBillStatus(int billId,String status);

    /**
     * Cập nhật thông tin của bill mới vào cơ sở dữ liệu
     *
     * @param bill Thông tin của bill cần cập nhật.
     */
    public void updateBill(Bill bill);

}
