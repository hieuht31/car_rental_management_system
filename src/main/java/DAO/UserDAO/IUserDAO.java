package DAO.UserDAO;

import java.util.List;

import Model.User;
import Model.BussinessModel.FilterEntity;
import Model.BussinessModel.ResponseEntity;

public interface IUserDAO {
	
	
	/**
	 * Tìm kiếm người dùng với các tiêu chí được định nghĩa trong đối tượng FilterEntity và trả về danh sách các ResponseEntity.
	 * 
	 * @param filterEntity đối tượng FilterEntity đại diện cho các tiêu chí tìm kiếm
	 * @return ResponseEntity đại diện cho các người dùng được tìm thấy
	 */
	public ResponseEntity findUser(FilterEntity filterEntity);
	
	/**
	 * Tìm kiếm người dùng với email được truyền vào và trả về đối tượng User.
	 * 
	 * @param email đối tượng FilterEntity đại diện cho các tiêu chí tìm kiếm
	 * @return User đại diện cho người dùng được tìm thấy
	 */
	public User findUserByEmail(String email);

    
	/**
     * Tìm kiếm người dùng với id được truyền vào và trả về đối tượng User.
     * 
     * @param userId la id dai dien cho user muon tim
     * @return User la người dùng được tìm thấy
     */
	public User findUserById(int userId);
	
	/**
     * Thêm một người dùng mới vào cơ sở dữ liệu.
     *
     * @param user Người dùng mới.
     * @ Nếu có lỗi khi truy vấn cơ sở dữ liệu.
     */
    void addUser(User user) ;

    /**
     * Cập nhật thông tin của một người dùng trong cơ sở dữ liệu.
     *
     * @param user Người dùng cần cập nhật.
     */
    void updateUser(User user);
    
    /**
     * User Cập nhật thông tin profile người dùng trong cơ sở dữ liệu.
     *
     * @param user Người dùng cần cập nhật.
     */
    void updateProfile(User user);

    /**
     * Update password by user id
     *
     * @param userId ID của người dùng cần xóa.
     */
    void updatePassword(User user);
    
    /**
     * Xóa một người dùng khỏi cơ sở dữ liệu.
     *
     * @param userId ID của người dùng cần xóa.
     */
    void deleteUser(int userId) ;
}
