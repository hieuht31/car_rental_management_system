package DAO.UserDAO.UserDAOImpl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DAO.DBConnection;
import DAO.UserDAO.IUserDAO;
import Model.User;
import Model.BussinessModel.FilterEntity;
import Model.BussinessModel.ResponseEntity;

public class UserDAOImpl extends DBConnection implements IUserDAO {
	PreparedStatement pstate;
	ResultSet rs;
	String sql;
	List<User> userList;
	

	@Override
	public ResponseEntity findUser(FilterEntity filterEntity) {
		int startRecord = filterEntity.getNumberRecord() * (filterEntity.getCurrentPage() - 1);
		ResponseEntity responseEntity = new ResponseEntity();
		responseEntity.setCurrentPage(filterEntity.getCurrentPage());
		userList = new ArrayList<>();
		
		String searchCondition = filterEntity.getSearchString().isEmpty()?"": "WHERE "+filterEntity.getSearchField()+" LIKE '%'+?+'%'";
		
		
		sql = "SELECT *, CEILING(COUNT(*) OVER()/5.0)[number_record] FROM users "+searchCondition
				+ " ORDER BY user_id"
				+ " OFFSET ? ROW FETCH NEXT ? ROWS ONLY";
		try {
			int index = 1;
			pstate = getConnection().prepareStatement(sql);
			if (!searchCondition.isEmpty())
			pstate.setString(index++, filterEntity.getSearchString());
			pstate.setInt(index++, startRecord);
			pstate.setInt(index++, filterEntity.getNumberRecord());
			
			rs = pstate.executeQuery();
			if(rs.next()) {
				userList.add(new User(rs.getInt(1),
						rs.getString(2),
						rs.getString(3),rs.getString(4),
						rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9)));
				responseEntity.setTotalPage(rs.getInt(10));
			}
			while(rs.next()) {
				userList.add(new User(rs.getInt(1),
						rs.getString(2),
						rs.getString(3),rs.getString(4),
						rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9)));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		responseEntity.setData(userList);
		return responseEntity;
	}

	@Override
	public User findUserByEmail(String email) {
		sql = "SELECT  * FROM users WHERE email = ? ";
		try {
			pstate = getConnection().prepareStatement(sql);
			pstate.setString(1, email);
			rs = pstate.executeQuery();
			if(rs.next())
				return new User(rs.getInt(1),
						rs.getString(2),
						rs.getString(3),rs.getString(4),
						rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public User findUserById(int userId) {
		sql = "SELECT  * FROM users WHERE user_id = ? ";
		try {
			pstate = getConnection().prepareStatement(sql);
			pstate.setInt(1, userId);
			rs = pstate.executeQuery();
			if(rs.next())
				return new User(rs.getInt(1),
						rs.getString(2),
						rs.getString(3),rs.getString(4),
						rs.getString(5),rs.getString(6)
						,rs.getString(7),rs.getString(8)
						,rs.getString(9));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void addUser(User user) {
		sql = "INSERT INTO users VALUES (?,?,?,?,?,?,?,?)";
		try {
			pstate = getConnection().prepareStatement(sql);
			pstate.setString(1, user.getFullName());
			pstate.setString(2, user.getDateOfBirth());
			pstate.setString(3, user.getAddress());
			pstate.setString(4, user.getPhoneNumber());
			pstate.setString(5, user.getEmail());
			pstate.setString(6, user.getPassword());
			pstate.setString(7, user.getRole());
			pstate.setString(8, user.getStatus());
			
			pstate.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Override
	public void updateProfile(User user) {
		// TODO Auto-generated method stub
		sql = "UPDATE [users]\r\n"
				+ "   SET [full_name] = ?\r\n"
				+ "      ,[dob] = ?\r\n"
				+ "      ,[address] = ?\r\n"
				+ "      ,[phone_number] = ?\r\n"
				+ " WHERE [user_id] = ?";
		try {
			pstate = getConnection().prepareStatement(sql);
			pstate.setString(1, user.getFullName());
			pstate.setString(2, user.getDateOfBirth());
			pstate.setString(3, user.getAddress());
			pstate.setString(4, user.getPhoneNumber());
			pstate.setInt(5, user.getUserId());
			
			pstate.executeUpdate();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}
	
	@Override
	public void updateUser(User user) {

		sql = "UPDATE [users]\r\n"
				+ "   SET [full_name] = ?\r\n"
				+ "      ,[dob] = ?\r\n"
				+ "      ,[address] = ?\r\n"
				+ "      ,[phone_number] = ?\r\n"
				+ "      ,[role] = ?\r\n"
				+ "      ,[status] = ?\r\n"
				+ " WHERE [user_id] = ?";
		try {
			pstate = getConnection().prepareStatement(sql);
			pstate.setString(1, user.getFullName());
			pstate.setString(2, user.getDateOfBirth());
			pstate.setString(3, user.getAddress());
			pstate.setString(4, user.getPhoneNumber());
			pstate.setString(5, user.getRole());
			pstate.setString(6, user.getStatus());
			pstate.setInt(7, user.getUserId());
			
			pstate.executeUpdate();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}


	@Override
	public void deleteUser(int userId) {
		sql = "DELETE FROM users WHERE user_id = ?";
		
		try {
			pstate = getConnection().prepareStatement(sql);
			pstate.setInt(1, userId);
			pstate.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			
		}

	}

	@Override
	public void updatePassword(User user) {
		sql = "UPDATE [users]\r\n"
				+ "   SET [password] = ?\r\n"
				+ " WHERE [user_id] = ?";
		try {
			pstate = getConnection().prepareStatement(sql);
			pstate.setString(1, user.getPassword());
			pstate.setInt(2, user.getUserId());
			
			pstate.executeUpdate();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		
	}

	

}
