package DAO.RentalContractDAO.RentalContractDAOImpl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import DAO.DBConnection;
import DAO.RentalContractDAO.IRentalContractDAO;
import Model.Car;
import Model.RentalContract;
import Model.RentalDetail;
import Model.User;
import Model.BussinessModel.FilterEntity;
import Model.BussinessModel.ResponseEntity;

public class RentalContractDAOImpl extends DBConnection implements IRentalContractDAO {
	PreparedStatement pstate;
	ResultSet rs;
	String sql;
	List<RentalContract> rentalContractList;
	
	
	@Override
	public ResponseEntity findAllContract(FilterEntity filterEntity) {
		int startRecord = filterEntity.getNumberRecord() * (filterEntity.getCurrentPage() - 1);
		ResponseEntity responseEntity = new ResponseEntity();
		responseEntity.setCurrentPage(filterEntity.getCurrentPage());
		String searchCondition = filterEntity.getSearchString().isEmpty()?"": " WHERE "+filterEntity.getSearchField()+" LIKE '%'+?+'%'";
		
		rentalContractList = new ArrayList<>();
		sql = "SELECT rental_contract.*,users.fullName, CEILING(CAST(COUNT(*) OVER() AS FLOAT)/?)[number_record]  FROM rental_contract INNER JOIN users "
				+ "ON rental_contract.user_id = users.user_id  "
				+searchCondition+" ORDER BY rental_contract_id "
				+ "OFFSET ? ROW FETCH NEXT ? ROWS ONLY";
		try {
			int index = 0;
			pstate = getConnection().prepareStatement(sql);
			pstate.setInt(index++, filterEntity.getNumberRecord());
			if (!searchCondition.isEmpty())
				pstate.setString(index++, filterEntity.getSearchString());
			pstate.setInt(index++, startRecord);
			pstate.setInt(index++, filterEntity.getNumberRecord());
			rs = pstate.executeQuery();
			if(rs.next()) {
				rentalContractList.add(new RentalContract(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4)
						,rs.getString(5),new User(rs.getInt(6),rs.getString(7))));
				responseEntity.setTotalPage(rs.getInt(8));
			}
			while (rs.next()) {
				rentalContractList.add(new RentalContract(rs.getInt(1),rs.getString(2)
										,rs.getString(3),rs.getString(4)
										,rs.getString(5),new User(rs.getInt(6)
										,rs.getString(7))));				
			}
			responseEntity.setData(rentalContractList);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return responseEntity;
	}

	@Override
	public List<RentalContract> findOnPrepareContract() {
		sql = "SELECT * FROM rental_contract WHERE status = 'OnPrepare'";
		rentalContractList = new ArrayList<>();
		try {
			pstate = getConnection().prepareStatement(sql);
			rs = pstate.executeQuery();
			while (rs.next()) {
				rentalContractList.add(new RentalContract(rs.getInt(1),rs.getString(2)
										,rs.getString(3),rs.getString(4)
										,rs.getString(5),new User(rs.getInt(6)
										,rs.getString(7))));				
			}
			return rentalContractList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@Override
	public ResponseEntity findPersonalContract(int userId, FilterEntity filterEntity) {
		int startRecord = filterEntity.getNumberRecord() * (filterEntity.getCurrentPage() - 1);
		ResponseEntity responseEntity = new ResponseEntity();
		responseEntity.setCurrentPage(filterEntity.getCurrentPage());
		String searchCondition = filterEntity.getSearchString().isEmpty()?"": " AND "+filterEntity.getSearchField()+" LIKE '%'+?+'%'";
		
		rentalContractList = new ArrayList<>();
		sql = "SELECT rental_contract.*,users.fullName, CEILING(CAST(COUNT(*) OVER() AS FLOAT)/?)[number_record]  FROM rental_contract INNER JOIN users "
				+ "ON rental_contract.user_id = users.user_id WHERE user_id = ? "
				+searchCondition+" ORDER BY rental_contract_id "
				+ "OFFSET ? ROW FETCH NEXT ? ROWS ONLY";
		try {
			int index = 0;
			pstate = getConnection().prepareStatement(sql);
			pstate.setInt(index++, filterEntity.getNumberRecord());
			pstate.setInt(index++, userId);
			if (!searchCondition.isEmpty())
				pstate.setString(index++, filterEntity.getSearchString());
			pstate.setInt(index++, startRecord);
			pstate.setInt(index++, filterEntity.getNumberRecord());
			rs = pstate.executeQuery();
			if(rs.next()) {
				rentalContractList.add(new RentalContract(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4)
						,rs.getString(5),new User(rs.getInt(6),rs.getString(7))));
				responseEntity.setTotalPage(rs.getInt(8));
			}
			while (rs.next()) {
				rentalContractList.add(new RentalContract(rs.getInt(1),rs.getString(2)
										,rs.getString(3),rs.getString(4)
										,rs.getString(5),new User(rs.getInt(6)
										,rs.getString(7))));				
			}
			responseEntity.setData(rentalContractList);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return responseEntity;
	}

	@Override
	public void addContract(RentalContract rentalContract) {
		sql = "INSERT INTO [rental_contract]"
				+ "           ([create_at]"
				+ "           ,[start_at]"
				+ "           ,[end_at]"
				+ "           ,[status]"
				+ "           ,[user_id])"
				+ "     VALUES\r\n"
				+ "           (?"
				+ "           ,?"
				+ "           ,?"
				+ "           ,?"
				+ "           ,?)";
		try {
			pstate = getConnection().prepareStatement(sql);
			pstate.setString(1, rentalContract.getCreateAt());
			pstate.setString(2, rentalContract.getStartDate());
			pstate.setString(3, rentalContract.getEndDate());
			pstate.setString(4, rentalContract.getStatus());
			pstate.setInt(5, rentalContract.getCustomer().getUserId());
			
			pstate.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Override
	public void updateRentalContract(RentalContract rentalContract) {
		sql = "UPDATE [rental_contract]"
				+ "   SET [create_at] = ?"
				+ "      ,[start_at] = ?"
				+ "      ,[end_at] = ?"
				+ "      ,[status] = ?"
				+ "      ,[user_id] = ?"
				+ " WHERE rental_contract_id = ?";
		try {
			pstate = getConnection().prepareStatement(sql);
			pstate.setString(1, rentalContract.getCreateAt());
			pstate.setString(2, rentalContract.getStartDate());
			pstate.setString(3, rentalContract.getEndDate());
			pstate.setString(4, rentalContract.getStatus());
			pstate.setInt(5, rentalContract.getCustomer().getUserId());
			
			pstate.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void updateContractStatus(int contractId, String status) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteRentalContract(int rentalContractId) {
		sql = "DELETE FROM rental_contract WHERE rental_contract_id = ?";
		
		try {
			pstate = getConnection().prepareStatement(sql);
			pstate.setInt(1, rentalContractId);
			pstate.executeQuery();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}


}
