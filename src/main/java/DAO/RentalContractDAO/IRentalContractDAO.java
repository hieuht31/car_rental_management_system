package DAO.RentalContractDAO;

import java.util.List;

import Model.Car;
import Model.RentalContract;
import Model.RentalDetail;
import Model.BussinessModel.FilterEntity;
import Model.BussinessModel.ResponseEntity;

public interface IRentalContractDAO {
	/**
	 * Tìm kiếm rental contract với các tiêu chí được định nghĩa trong đối tượng FilterEntity và trả về danh sách các ResponseEntity.
	 * 
	 * @param filterEntity đối tượng FilterEntity đại diện cho các tiêu chí tìm kiếm
	 * @return List<ResponseEntity> đại diện cho các contract được tìm thấy
	 */
	public ResponseEntity findAllContract(FilterEntity filterEntity);
	
	/**
	 * Tìm kiếm rental contract với các tiêu chí được định nghĩa trong FilterEntity và cutomer id 
	 * trong đối tượng FilterEntity và trả về danh sách các ResponseEntity.
	 * 
	 * @param userId giá trị xác định khách hàng của hợp đồng cho thuê
	 * @param filterEntity đối tượng FilterEntity đại diện cho các tiêu chí tìm kiếm
	 * @return List<ResponseEntity> đại diện cho các rental contract được tìm thấy
	 */
	public ResponseEntity findPersonalContract(int userId,FilterEntity filterValue);
	
	/**
	 * Tìm kiếm rental contract có trạng thái OnPrepare và trả về list các contract
	 * 
	 * @param userId giá trị xác định khách hàng của hợp đồng cho thuê
	 * @param filterEntity đối tượng FilterEntity đại diện cho các tiêu chí tìm kiếm
	 * @return List<ResponseEntity> đại diện cho các rental contract được tìm thấy
	 */
	public List<RentalContract> findOnPrepareContract();

    /**
     * Thêm một hợp đồng mới vào cơ sở dữ liệu.
     *
     * @param car Người dùng mới.
     * @ Nếu có lỗi khi truy vấn cơ sở dữ liệu.
     */
    void addContract(RentalContract rentalContract) ;

    /**
     * Cập nhật thông tin của hợp đồng trong cơ sở dữ liệu.
     *
     * @param car Car cần cập nhật.
     */
    void updateRentalContract(RentalContract rentalContract);
    
    /**
     * Cập nhật trang thai của Rental Contract trong cơ sở dữ liệu.
     *
     * @param contract Id của Contract cần cập nhật.
     */
    void updateContractStatus(int contractId,String status) ;

    /**
     * Xóa một người dùng khỏi cơ sở dữ liệu.
     *
     * @param user Người dùng cần xóa.
     */
    void deleteRentalContract(int rentalContractId) ;

}
