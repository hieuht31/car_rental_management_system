package Model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int userId;
	private String fullName;
	private String dateOfBirth;
	private String address;
	private String phoneNumber;
	private String email;
	private String password;
	private String role;
	private String status;
	
	public User(int userId, String fullName) {
		this.userId = userId;
		this.fullName = fullName;
	}
}
