package Model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;



@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
// Chua thong tin ve cac khoan thu cua 1 xe cho thue: tien coc, tien phu thu
public class FeeRecord implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int feeRecordId;
	private int feeId;
	private String feeDate;
	private String feeType;
	private double amount;
	private String status;
	private RentalDetail rentalDetail;

}
