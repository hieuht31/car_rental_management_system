package Model.BussinessModel;

import java.io.Serializable;

import Model.Car;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FilterEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String searchString;
	private String searchField;
	private int currentPage;
	private int numberRecord;
}
