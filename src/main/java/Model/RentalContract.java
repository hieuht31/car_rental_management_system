package Model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;



@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RentalContract implements Serializable{
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int rentalContract;
	 private String createAt;
	 private String startDate;
	 private String endDate;
	 private String status;
	 private User customer;

}
