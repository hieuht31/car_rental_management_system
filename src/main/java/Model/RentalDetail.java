package Model;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RentalDetail implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int rentalDetail;
	private List<Car> listCar;
	private RentalContract rentailContract;
		
}
