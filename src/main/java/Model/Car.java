package Model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Car implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int carId;
	private String licensePlate;
	private String carType;
	private String carColor;
	private String carBrand;
	private double rentalRate;
	private int capacity;
	private String status;

}
