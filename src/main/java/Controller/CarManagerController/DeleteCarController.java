package Controller.CarManagerController;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import DAO.CarDAO.ICarDAO;
import DAO.CarDAO.Impl.CarDAOImpl;
import Model.Car;
import Model.BussinessModel.ServiceResponse;

/**
 * Servlet implementation class DeleteCarManager
 */
@WebServlet("/car/delete")
public class DeleteCarController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteCarController() {
		super();
		// TODO Auto-generated constructor stub
	}

	private ICarDAO carDAO;

	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		carDAO = new CarDAOImpl();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json; charset=UTF-8");

		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		int carId = Integer.parseInt(request.getParameter("carId").isEmpty() ? "0" : request.getParameter("carId"));

		carDAO.deleteCar(new Car(carId, "", "", "", "", 0, 0, ""));
		serviceResponse.setSuccess(true);
		serviceResponse.setMessage("Delete car success!");

		out.print(gson.toJson(serviceResponse));
	}

}
