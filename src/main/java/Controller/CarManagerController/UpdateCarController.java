package Controller.CarManagerController;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import DAO.CarDAO.ICarDAO;
import DAO.CarDAO.Impl.CarDAOImpl;
import Model.Car;
import Model.BussinessModel.ServiceResponse;

/**
 * Servlet implementation class UpdateCarController
 */
@WebServlet("/car/update")
public class UpdateCarController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ICarDAO carDAO;
	private int carId;
	private String licensePlate;
	private String carType;
	private String carColor;
	private String carBrand;
	private double rentalRate;
	private int capacity;
	private String status;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateCarController() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		carDAO = new CarDAOImpl();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		carId = Integer.parseInt(request.getParameter("carId").isEmpty() ? "0" : request.getParameter("carId"));
		Car car = carDAO.findCarById(carId);
		request.setAttribute("car", car);
		request.getRequestDispatcher("../Views/View_CarManagement/CarUpdate.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json; charset=UTF-8");
		

		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		carId = Integer.parseInt(request.getParameter("carId").isEmpty() ? "0" : request.getParameter("carId"));
		licensePlate = request.getParameter("licensePlate");
		carType = request.getParameter("carType");
		carColor = request.getParameter("carColor");
		carBrand = request.getParameter("carBrand");
		rentalRate = Double.parseDouble(request.getParameter("rentalRate"));
		capacity = Integer.parseInt(request.getParameter("capacity"));
		status = request.getParameter("status");
		if (carDAO.findCarByLisencePlate(licensePlate) != null) {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage("Lisence plate has exist!");
		} else {
			carDAO.updateCar(new Car(carId, licensePlate, carType, carColor, carBrand, rentalRate, capacity, status));
			serviceResponse.setSuccess(true);
			serviceResponse.setMessage("Update car success!");			
		}

		out.print(gson.toJson(serviceResponse));
	}

}
