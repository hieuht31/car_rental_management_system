package Controller.RentalContractController;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import DAO.RentalContractDAO.IRentalContractDAO;
import DAO.RentalContractDAO.RentalContractDAOImpl.RentalContractDAOImpl;
import Model.BussinessModel.FilterEntity;
import Model.BussinessModel.ResponseEntity;
import Model.BussinessModel.ServiceResponse;


/**
 * Servlet implementation class ContractListController
 */
@WebServlet("/contract/list")
public class ContractListController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ContractListController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("../Views/RentalContractManagement/ListRentalContract.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		FilterEntity filterEntity = new FilterEntity();
		IRentalContractDAO contractDAO = new RentalContractDAOImpl();
		try {
			filterEntity.setCurrentPage(Integer.parseInt(request.getParameter("currentPage")));
			filterEntity.setNumberRecord(Integer.parseInt(request.getParameter("numberRecord")));
			filterEntity.setSearchField(request.getParameter("filterBy"));
			filterEntity.setSearchString(request.getParameter("searchValue"));
			serviceResponse.setData(contractDAO.findAllContract(filterEntity));
			serviceResponse.setSuccess(true);

		} catch (Exception e) {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage(e.getMessage());
		}
		out.print(gson.toJson(serviceResponse));
	}

}
