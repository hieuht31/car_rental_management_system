package Controller.AccountController;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.UserDAO.IUserDAO;
import DAO.UserDAO.UserDAOImpl.UserDAOImpl;
import Model.User;

/**
 * Servlet implementation class Register
 */
@WebServlet("/register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("Views/CommonView/Register.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		IUserDAO userDAO = new UserDAOImpl();
		if(!request.getParameter("password").equals(request.getParameter("confirmPassword"))) {
			request.setAttribute("errorMessage", "Confirm password not match!");
			request.getRequestDispatcher("Views/CommonView/Register.jsp").forward(request, response);
		} else {
			if(userDAO.findUserByEmail(request.getParameter("email"))!=null) {
				request.setAttribute("errorMessage", "Email has exist!");
				request.getRequestDispatcher("Views/CommonView/Register.jsp")
				.forward(request, response);				
			} else {
				User user = new User(0,request.getParameter("fullName"),request.getParameter("dateOfBirth"),request.getParameter("address"),request.getParameter("phoneNumber")
						,request.getParameter("email"),request.getParameter("password"),"Customer","Active");
				
				new UserDAOImpl().addUser(user);	
				response.sendRedirect("login");				
			}
		}
		
		
	}

}
