package Controller.AccountController;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import DAO.UserDAO.UserDAOImpl.UserDAOImpl;
import Model.User;
import Model.BussinessModel.ServiceResponse;

/**
 * Servlet implementation class UserProfileController
 */
@WebServlet({"/account/profile","/account/update","/account/delete"})
public class UserProfileController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserProfileController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int userId = ((User)request.getSession().getAttribute("user")).getUserId();

		User user_info = new UserDAOImpl().findUserById(userId);
		if(user_info==null) request.getRequestDispatcher("../Views/CommonView/AccessDenied.jsp").forward(request, response);
		else {
			request.setAttribute("user_info", user_info);
			request.getRequestDispatcher("../Views/CommonView/UserProfile.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		response.setContentType("application/json; charset=UTF-8");
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		request.setCharacterEncoding("UTF-8");
		if (request.getServletPath().contains("update")) {
			int userId = Integer.parseInt(request.getParameter("userId"));
			
			String fullName = request.getParameter("fullName");
			String dateOfBirth = request.getParameter("dateOfBirth");
			String phoneNumber = request.getParameter("phoneNumber");
			String address = request.getParameter("address");
			
			new UserDAOImpl().updateProfile(new User(userId, fullName, dateOfBirth, address, phoneNumber, "", "", "", ""));
			serviceResponse.setMessage("Update user info successful");
			serviceResponse.setSuccess(true);
			
		} else if (request.getServletPath().contains("delete")) {
			int userId = Integer.parseInt(request.getParameter("userId"));
			new UserDAOImpl().deleteUser(userId);
			serviceResponse.setMessage("Delete user successfully!");
			serviceResponse.setSuccess(true);
		}
		out.print(gson.toJson(serviceResponse));

		
	}

}
