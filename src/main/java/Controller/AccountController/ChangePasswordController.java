package Controller.AccountController;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import DAO.UserDAO.UserDAOImpl.UserDAOImpl;
import Model.User;
import Model.BussinessModel.ServiceResponse;

/**
 * Servlet implementation class ChangePasswordController
 */
@WebServlet("/account/change_password")
public class ChangePasswordController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ChangePasswordController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("../Views/CommonView/ChangePassword.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json; charset=UTF-8");
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		request.setCharacterEncoding("UTF-8");
		
		User user = (User)request.getSession().getAttribute("user");
		int userId = Integer.parseInt(request.getParameter("userId"));

		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");
		if(!(user.getPassword().equals(request.getParameter("currentPassword")))) {
			
			serviceResponse.setMessage("Current pass is not match");
			serviceResponse.setSuccess(false);
		}else if(password.equals(confirmPassword)) {
			user.setPassword(password);
			User user_update = new User(); 
			user_update.setUserId(userId);
			user_update.setPassword(password);
			new UserDAOImpl().updatePassword(user_update);
			request.getSession().setAttribute("user", user);
			serviceResponse.setMessage("Update user info successful");
			serviceResponse.setSuccess(true);			
		} else {
			serviceResponse.setMessage("Comfirm pass is not match");
			serviceResponse.setSuccess(false);			
		}

		out.print(gson.toJson(serviceResponse));
	}

}
