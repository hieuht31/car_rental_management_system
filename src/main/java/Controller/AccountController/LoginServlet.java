package Controller.AccountController;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.UserDAO.UserDAOImpl.UserDAOImpl;
import Model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("Views/CommonView/SignIn.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		User user = new UserDAOImpl().findUserByEmail(email);
		String responseMessage="";
		
		if(user == null) {
			responseMessage = "Email not exist!";
			request.setAttribute("errorMessage", responseMessage);
			request.getRequestDispatcher("Views/CommonView/SignIn.jsp").forward(request, response);
		} else if(!user.getPassword().equals(password)) {
			responseMessage = "Password is incorrect!";
			request.setAttribute("errorMessage", responseMessage);
			request.getRequestDispatcher("Views/CommonView/SignIn.jsp").forward(request, response);
		} else if(user.getStatus().equals("InActive")) {
			responseMessage = "Your account is inactive";
			request.setAttribute("warningMessage", responseMessage);
			request.getRequestDispatcher("Views/CommonView/SignIn.jsp").forward(request, response);
		} else if (user.getRole().equals("Admin")) {
			// TODO: redirect to admin home page (List user)
			request.getSession().setAttribute("user", user);
			response.sendRedirect("user/list");
		} else if(user.getRole().equals("Staff")) {
			request.getSession().setAttribute("user", user);
			response.sendRedirect("car/list");
			
			// TODO: redirect to staff home page(List car)
		} else if (user.getRole().equals("Customer")) {
			request.getSession().setAttribute("user", user);
			response.sendRedirect("car/list");
		}
	}

}
