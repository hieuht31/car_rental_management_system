package Controller.UserManagerController;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import DAO.UserDAO.IUserDAO;
import DAO.UserDAO.UserDAOImpl.UserDAOImpl;
import Model.User;
import Model.BussinessModel.ServiceResponse;

/**
 * Servlet implementation class UserUpdateController
 */
@WebServlet("/user/update")
public class UserUpdateController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private IUserDAO userDAO;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateController() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    	super.init();
    	userDAO = new UserDAOImpl();
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		User user_update = userDAO.findUserById(Integer.parseInt(request.getParameter("userId")));
		request.setAttribute("user_update", user_update);
		request.getRequestDispatcher("../Views/UserManagement/UpdateUser.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json; charset=UTF-8");
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		request.setCharacterEncoding("UTF-8");

		int userId = Integer.parseInt(request.getParameter("userId"));
		String fullName = request.getParameter("fullName");
		String dateOfBirth = request.getParameter("dateOfBirth");
		String phoneNumber = request.getParameter("phoneNumber");
		String address = request.getParameter("address");
		String role = request.getParameter("role");
		String status = request.getParameter("status");

		new UserDAOImpl().updateProfile(new User(userId, fullName, dateOfBirth, address, phoneNumber, "", "", role, status));
		serviceResponse.setMessage("Update user info successful");
		serviceResponse.setSuccess(true);

		out.print(gson.toJson(serviceResponse));
	}

}
