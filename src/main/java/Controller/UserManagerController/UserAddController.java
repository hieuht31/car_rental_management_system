package Controller.UserManagerController;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import DAO.UserDAO.IUserDAO;
import DAO.UserDAO.UserDAOImpl.UserDAOImpl;
import Model.User;
import Model.BussinessModel.ServiceResponse;

/**
 * Servlet implementation class UserAddController
 */
@WebServlet("/user/add")
public class UserAddController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserAddController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("../Views/UserManagement/AddUser.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		IUserDAO userDAO = new UserDAOImpl();

		String fullName = request.getParameter("fullName");

		String dateOfBirth = request.getParameter("dateOfBirth");
		String address = request.getParameter("address");
		String phoneNumber = request.getParameter("phoneNumber");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String role = request.getParameter("role");
		String status = request.getParameter("status").isEmpty() ? "Active" : request.getParameter("status");

		if (userDAO.findUserByEmail(email) != null) {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage("Email has exist!");
		} else {
			userDAO.addUser(new User(0, fullName, dateOfBirth, address, phoneNumber, email, password, role, status));
			serviceResponse.setSuccess(true);
			serviceResponse.setMessage("User added into list users");
		}

		out.print(gson.toJson(serviceResponse));
	}

}
