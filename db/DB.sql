--CREATE DATABASE CRMS_DB;
--USE CRMS_DB


CREATE TABLE [users](
	[user_id] INT PRIMARY KEY IDENTITY(1,1),
	full_name NVARCHAR(56),
	dob VARCHAR(10),
	[address] VARCHAR(56),
	phone_number VARCHAR(10),
	email VARCHAR(50) UNIQUE,
	[password] VARCHAR(50),
	[role] VARCHAR(50),
	[status] VARCHAR(50)
)

SELECT  *, CEILING((COUNT(*) OVER()/5.0))[number_page] FROM users
ORDER BY [user_id]
OFFSET 0 ROW FETCH NEXT 5 ROWS ONLY

INSERT INTO users VALUES
(N'Quan ly mot','01-01-2001', 'Ha Noi','0987654321','admin01@gmail.com','Admin01','Admin','Active'),
(N'Nhan Vien 1','01-01-2001', 'Ha Noi','0987654312','staff01@gmail.com','Staff001','Staff','Active'),
(N'Nhan Vien 2','01-01-2001', 'Ha Noi','0987654313','staff02@gmail.com','Staff002','Staff','Active'),
(N'Nhan Vien 3','01-01-2001', 'Ha Noi','0987654314','staff03@gmail.com','Staff003','Staff','Active'),
(N'Nhan Vien 4','01-01-2001', 'Ha Noi','0987654315','staff04@gmail.com','Staff004','Staff','Active'),
(N'Khach Hang 1','01-01-2001', 'Ha Noi','0987654112','customer01@gmail.com','Customer001','Customer','Active'),
(N'Khach Hang 2','01-01-2001', 'Ha Noi','0987654212','customer02@gmail.com','Customer002','Customer','Active'),
(N'Khach Hang 3','01-01-2001', 'Ha Noi','0987654412','customer03@gmail.com','Customer003','Customer','Active')

Update users set full_name = N'Nhan Thit Vien', dob = '2001-01-01',[address] = 'HN'Where [user_id] =2 

--List car type
--Economy Cars: These are small, fuel-efficient cars that are ideal for city driving or short trips. They usually seat four people and have a small trunk space.

--Compact Cars: These are slightly larger than economy cars and offer more legroom and luggage space. They are still fuel-efficient and are a good choice for small families or couples.

--Mid-Size Cars: These are larger than compact cars and have more room for passengers and luggage. They are good for longer trips and offer a more comfortable ride.

--Full-Size Cars: These are larger than mid-size cars and are often used for business travel or family vacations. They have plenty of room for passengers and luggage, and offer a comfortable ride.

--Luxury Cars: These are high-end vehicles that offer top-of-the-line features and amenities. They are often rented for special occasions or for business travel.

--SUVs: Sport Utility Vehicles are popular for their spacious interior and ability to handle various types of terrain. They are a great choice for families or groups traveling with a lot of luggage or equipment.

--Vans: These are ideal for large groups or families traveling together. They have plenty of seating and luggage space and are often used for long road trips or vacations.

--Convertibles: These cars have a retractable roof and are ideal for driving in warm weather. They are often rented for special occasions or for leisurely drives along scenic routes.

--Here are some popular car rental brands that you can rent:

--Avis
--Budget
--Enterprise
--Hertz
--National
--Alamo
--Thrifty
--Sixt
--Europcar
--Dollar
--Payless
--Advantage
--Fox Rent a Car
--Rent-A-Wreck
--Zipcar (car sharing service)

CREATE TABLE car(
	car_id INT PRIMARY KEY IDENTITY(1,1),
	lisence_plate VARCHAR(12) UNIQUE,
	car_type VARCHAR(50),
	car_color VARCHAR(50),
	car_brand VARCHAR(50),
	rental_rate FLOAT,
	seating_capacity INT,
	[status] VARCHAR(10)
)

select * from car
ALTER TABLE CAR
ADD [status] VARCHAR(10)

INSERT INTO car VALUES ('29A-230.53','Compact Cars','Black','Europcar',20,4,'Active')
INSERT INTO car VALUES ('29A-230.52','Mid-Size Cars','Black','Advantage',21,4,'Active')
INSERT INTO car VALUES ('29A-230.51','Convertibles','Black','Payless',22,4,'Active')
INSERT INTO car VALUES ('29A-230.54','Full-Size Cars','Black','Budget',23,4,'Active')
INSERT INTO car VALUES ('29A-230.55','Compact Cars','Black','Dollar',24,4,'Active')
INSERT INTO car VALUES ('29A-230.56','Convertibles','Black','Europcar',25,4,'Active')
INSERT INTO car VALUES ('29A-230.57','Compact Cars','Black','Zipcar',15,4,'Active')
INSERT INTO car VALUES ('29A-230.63','Compact Cars','Black','Europcar',17,4,'Active')
INSERT INTO car VALUES ('29A-230.62','Mid-Size Cars','Black','Advantage',18,4,'Active')
INSERT INTO car VALUES ('29A-230.61','Convertibles','Black','Payless',23,4,'Active')
INSERT INTO car VALUES ('29A-230.64','Full-Size Cars','Black','Budget',26,4,'Active')
INSERT INTO car VALUES ('29A-230.65','Compact Cars','Black','Dollar',27,4,'Active')
INSERT INTO car VALUES ('29A-230.66','Convertibles','Black','Europcar',21,4,'Active')
INSERT INTO car VALUES ('29A-230.67','Compact Cars','Black','Zipcar',29,4,'Active')
INSERT INTO car VALUES ('29A-230.13','Compact Cars','Black','Europcar',20,4,'Active')
INSERT INTO car VALUES ('29A-230.12','Mid-Size Cars','Black','Advantage',21,4,'Active')
INSERT INTO car VALUES ('29A-230.11','Convertibles','Black','Payless',22,4,'Active')
INSERT INTO car VALUES ('29A-230.14','Full-Size Cars','Black','Budget',23,4,'Active')
INSERT INTO car VALUES ('29A-230.15','Compact Cars','Black','Dollar',24,4,'Active')
INSERT INTO car VALUES ('29A-230.16','Convertibles','Black','Europcar',25,4,'Active')
INSERT INTO car VALUES ('29A-230.17','Compact Cars','Black','Zipcar',15,4,'Active')
INSERT INTO car VALUES ('29A-230.23','Compact Cars','Black','Europcar',17,4,'Active')
INSERT INTO car VALUES ('29A-230.22','Mid-Size Cars','Black','Advantage',18,4,'Active')
INSERT INTO car VALUES ('29A-230.21','Convertibles','Black','Payless',23,4,'Active')
INSERT INTO car VALUES ('29A-230.24','Full-Size Cars','Black','Budget',26,4,'Active')
INSERT INTO car VALUES ('29A-230.25','Compact Cars','Black','Dollar',27,4,'Active')
INSERT INTO car VALUES ('29A-230.26','Convertibles','Black','Europcar',21,4,'Active')
INSERT INTO car VALUES ('29A-230.27','Compact Cars','Black','Zipcar',29,4,'Active')

CREATE TABLE rental_contract(
	rental_contract_id INT PRIMARY KEY IDENTITY(1,1),
	create_at VARCHAR(20),
	start_at VARCHAR(20),
	end_at VARCHAR(20),
	[status] VARCHAR(20),
	[user_id] INT REFERENCES [users]([user_id]) ON UPDATE CASCADE ON DELETE CASCADE
)

CREATE TABLE bill(
	bill_id INT PRIMARY KEY IDENTITY(1,1),
	issue_date VARCHAR(20),
	total_amount FLOAT,
	rental_contract_id INT REFERENCES rental_contract(rental_contract_id) ON UPDATE CASCADE ON DELETE CASCADE
)

CREATE TABLE rental_detail(
	rental_id INT PRIMARY KEY IDENTITY(1,1),
	unit_price FLOAT,
	car_id INT REFERENCES car(car_id) ON UPDATE CASCADE ON DELETE CASCADE,
	rentail_contract_id INT REFERENCES rental_contract(rental_contract_id) ON UPDATE CASCADE ON DELETE CASCADE
)

SELECT *, CEILING((CAST(COUNT(*) OVER() AS Float))/5)[number_record] FROM car  ORDER BY car_id 
OFFSET 0 ROW FETCH NEXT 5 ROWS ONLY


CREATE TABLE fee_record(
	fee_id INT PRIMARY KEY IDENTITY(1,1),
	fee_date VARCHAR(20),
	fee_type VARCHAR(100),
	amount FLOAT,
	[status] VARCHAR(20),
	rental_id INT REFERENCES rental_detail(rental_id) ON UPDATE CASCADE ON DELETE CASCADE
)
GO;

CREATE TRIGGER update_car_status
ON rental_detail
FOR INSERT, UPDATE, DELETE
AS
BEGIN
	-- Update car status where a new record inserted, updated or deleted rental detail
	DECLARE @car_update_active_id INT, @car_update_rented_id INT

	SELECT @car_update_active_id = ISNULL(d.car_id,0) FROM deleted d
	
	SELECT @car_update_rented_id = ISNULL(i.car_id,0) FROM inserted i 
	
	IF(@car_update_active_id >0)
	BEGIN 
		UPDATE car SET status = 'Active' WHERE car_id = @car_update_active_id AND [status] = 'Rented'
	END
	-- if add car, update park_id then check park_status 
	IF(@car_update_rented_id > 0)
	BEGIN 
		UPDATE car SET [status] = 'Rented' WHERE car_id = @car_update_rented_id
	END
END
GO;
SELECT * FROM car

--CREATE TRIGGER update_car_status_of_complete_contract
--ON rental_contract
--FOR UPDATE
--AS
--BEGIN
--	-- Update car status where a new record inserted, updated or deleted rental detail
--	DECLARE @complete_contract_id int, @reset_contract_id INT

--	SELECT @complete_contract_id = ISNULL(i.rental_contract_id,0) FROM inserted i WHERE [status]='Complete'
--	SELECT @reset_contract_id = ISNULL(d.rental_contract_id,0) FROM deleted d WHERE [status]='Complete'

--	IF(@complete_contract_id>0) UPDATE car set [status] = 'Active' 
--	WHERE car_id IN (SELECT  car_id FROM rental_detail 
--	WHERE rentail_contract_id = @complete_contract_id) AND [status] = 'Rented'
	
--	IF (@reset_contract_id>0) UPDATE car set [status] = 'Rented' 
--	WHERE car_id IN (SELECT  car_id FROM rental_detail 
--	WHERE rentail_contract_id = @complete_contract_id) AND [status] = 'Active'
--END